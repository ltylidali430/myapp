Array.prototype.remove = function(s) {
    for (var i = 0; i < this.length; i++) {
        if (s == this[i])
            this.splice(i, 1);
    }
}
if (typeof Array.prototype.indexOf !== 'function') {
    Array.prototype.indexOf = function (item) {
        for(var i = 0; i < this.length; i++) {
            if (this[i] === item) {
                return i;
            }
        }
        return -1;
    };
}
/**
 * Simple MyDom
 * 
 * @author litianyu
 * @date 20151215
 * @class MyDom
 * @version 20151215
 */
function MyDom(els) {
    for(var i = 0; i < els.length; i++ ) {
        this[i] = els[i];
    }

    this.length = els.length;
    /**
    * @description 
    * Traverse elements
    * @return {Object}
    */
    this.forEach = function (callback) {
        this.map(callback);
        return this; 
    };
    /**
    * @description 
    * Returns the elements combined with one or more
    * @return {Object}
    */
    this.map = function (callback) {
        var results = [];
        for (var i = 0; i < this.length; i++) {
            results.push(callback.call(this, this[i], i));
        }
        return results;
    };
    /**
    * @description 
    * Returns the elements combined with one or more
    * @return {Object}
    */
    this.mapOne = function (callback) {
        var m = this.map(callback);
        return m.length > 1 ? m : m[0];
    };
    /**
    * @description 
    * value() method returns or sets the selected elements of the value attribute.
    * When used to return values:
    * This method returns the first matching element's value attribute value.
    * When used to set the value:
    * The method to set the value attribute values of all matched elements.
    * @returns {String}
    */
    this.value = function (text) {
        if (typeof text !== "undefined") {
            return this.forEach(function (el) {
                el.value = text;
            });
        } else {
            return this.mapOne(function (el) {
                return el.value;
            });
        }
    };

    /**
    * @description 
    * The text () method sets or returns the selected element text content.
    * When this method is used to return to the content, it returns the text contents of all matched elements (removes HTML tags).
    * When this method is used to set the content, then rewrite the contents of all matched elements
    * @returns {String}
    */
    this.text = function (text) {
        if (typeof text !== "undefined") {
            return this.forEach(function (el) {
                el.innerText = text;
            });
        } else {
            return this.mapOne(function (el) {
                return el.innerText;
            });
        }
    };
    /**
    * @description 
    * The HTML () method sets or returns the contents of the selected elements (innerHTML).
    * When this method is used to return to the content, it returns the contents of the first matched element.
    * When this method is used to set the content, then rewrite the contents of all matched elements
    * @returns {String}
    */
    this.html = function (html) {
        if (typeof html !== "undefined") {
            return this.forEach(function (el) {
                el.innerHTML = html;
            });
        } else {
            return this.mapOne(function (el) {
                return el.innerHTML;
            });
        }
    };
    /**
    * @description 
    * When the animation style changes, for each element in the set of matched elements to add the specified Class
    * 
    */
    this.addClass = function (classes) {
        var className = "";
        if (typeof classes !== 'string') {
            for (var i = 0; i < classes.length; i++) {
               className += " " + classes[i];
            }
        } else {
            className = " " + classes;
        }
        return this.forEach(function (el) {
            el.className += className;
        });
    };
    /**
    * @description 
    * When the animation style changes, for each element in the set of matched elements to remove the specified Class
    */
    this.removeClass = function (clazz) {
        return this.forEach(function (el) {
            var cs = el.className.split(' '), i;

            while ( (i = cs.indexOf(clazz)) > -1) { 
                cs = cs.slice(0, i).concat(cs.slice(++i));
            }
            el.className = cs.join(' ');
        });
    };
    /**
    * @description 
    * Get or Set elements attribute
    * @returns {String} attribute
    */
    this.attr = function (attr, val) {
        if (typeof val !== 'undefined') {
            return this.forEach(function(el) {
                el.setAttribute(attr, val);
            });
        } else {
            return this.mapOne(function (el) {
                return el.getAttribute(attr);
            });
        }
    };
    /**
    * @description Insert content elements at the end
    * 
    */
    this.append = function (els) {
        return this.forEach(function (parEl, i) {
            els.forEach(function (childEl) {
                parEl.appendChild( (i > 0) ? childEl.cloneNode(true) : childEl);
            });
        });
    };
    /**
    * @description 
    * Insert content element to the beginning
    * 
    */
    this.prepend = function (els) {
        return this.forEach(function (parEl, i) {
            for (var j = els.length -1; j > -1; j--) {
                parEl.insertBefore((i > 0) ? els[j].cloneNode(true) : els[j], parEl.firstChild);
            }
        });
    };
    /**
    * @description Remove elements
    * @returns Such as deleted successfully, this method returns the removed node, such as failed, it returns NULL
    */
    this.remove = function () {
        return this.forEach(function (el) {
            return el.parentNode.removeChild(el);
        });
    };
    /**
    * @description 
    * Element to add the event handler
    *
    */
    this.on = (function () {
        if (document.addEventListener) {
            return function (evt, fn) {
                return this.forEach(function (el) {
                    el.addEventListener(evt, fn, false);
                });
            };
        } else if (document.attachEvent)  {
            return function (evt, fn) {
                return this.forEach(function (el) {
                    el.attachEvent("on" + evt, fn);
                });
            };
        } else {
            return function (evt, fn) {
                return this.forEach(function (el) {
                    el["on" + evt] = fn;
                });
            };
        }
    }());
    /**
    * @description 
    * Removed by on () method to add the event handler
    *
    */
    this.off = (function () {
        if (document.removeEventListener) {
            return function (evt, fn) {
                return this.forEach(function (el) {
                    el.removeEventListener(evt, fn, false);
                });
            };
        } else if (document.detachEvent)  {
            return function (evt, fn) {
                return this.forEach(function (el) {
                    el.detachEvent("on" + evt, fn);
                });
            };
        } else {
            return function (evt, fn) {
                return this.forEach(function (el) {
                    el["on" + evt] = null;
                });
            };
        }
    }());
}
/**
 * Simple map
 * @class MyMap
 * @version 20151215
 * @example
 * var map = new MyMap();
 * map.put('key','value');
 * ...
 * var str = "";
 * map.each(function(key,value,index){
 *         str += index+":"+ key+"="+value+"\n";
 * });
 * alert(str);
 * 
 * @author litianyu
 * @date 20151215
 */
function MyMap() {
    /** @description Store key array (traversal used) */
    this.keys = new Array();
    /** @description To store data */
    this.data = new Object();
    
    /**
     * @description Put a key/value pair
     * @param {String} key
     * @param {Object} value
     */
    this.put = function(key, value) {
        if(this.data[key] == null){
            this.keys.push(key);
        }
        this.data[key] = value;
    };
    
    /**
     * @description To obtain a key value
     * @param {String} key
     * @returns {Object} value
     */
    this.get = function(key) {
        return this.data[key];
    };
    
    /**
     * @description To delete a key/value pair
     * @param {String} key
     */
    this.remove = function(key) {
        this.keys.remove(key);
        this.data[key] = null;
    };
    
    /**
     * @description Traverse Map, perform processing function
     * 
     * @param {Function} callback function(key,value,index){..}
     */
    this.each = function(fn){
        if(typeof fn != 'function'){
            return;
        }
        var len = this.keys.length;
        for(var i=0;i<len;i++){
            var k = this.keys[i];
            fn(k,this.data[k],i);
        }
    };
    
    /**
     * @description Get keys array (similar to Java entrySet ())
     * @returns Key/value object {key, value} array
     */
    this.entrys = function() {
        var len = this.keys.length;
        var entrys = new Array(len);
        for (var i = 0; i < len; i++) {
            entrys[i] = {
                key : this.keys[i],
                value : this.data[i]
            };
        }
        return entrys;
    };
    
    /**
     * @description To judge whether the Map is empty
     * @return true or false
     */
    this.isEmpty = function() {
        return this.keys.length == 0;
    };
    
    /**
     * @description Get number of key-value pairs
     * @returns {Number}
     */
    this.size = function(){
        return this.keys.length;
    };
    
    /**
     * @description Override the toString
     */
    this.toString = function(){
        var s = "{";
        for(var i=0;i<this.keys.length;i++,s+=','){
            var k = this.keys[i];
            s += k+"="+this.data[k];
        }
        s+="}";
        return s;
    };
}
/**
 * Simple map
 * @class myapp
 * @author litianyu
 * @version 20151215
 * @example
 * var app = new myapp();
 * var u = app.get('#username');
 * alert(u.value());
 * alert(u.text());
 * alert(u.html());
 * @date 20151215
 */
function myapp(){
    /**
    * @description 
    * @Returns {MyMap}
    */
    this.map = function(){
        return new MyMap();
    };
    /**
     * @description 
     * Select element or elements of the page, when there is no object to return
     * @Returns {MyDom}
     */
    this.get = function (selector) {
        var els;
        if (typeof selector === 'string') {
            els = document.querySelectorAll(selector);
        } else if (selector.length) {
            els = selector;
        } else {
            els = [selector];
        }
        return new MyDom(els);
    };
    /**
     * @description Create the element
     * @return {Object}
     */
    this.create = function (tagName, attrs) {
        var el = new MyDom([document.createElement(tagName)]);
        if (attrs) {
            if (attrs.className) { 
                el.addClass(attrs.className);
                delete attrs.className;
            }
            if (attrs.text) { 
                el.text(attrs.text);
                delete attrs.text;
            }
            for (var key in attrs) {
                if (attrs.hasOwnProperty(key)) {
                    el.attr(key, attrs[key]);
                }
            }
        }
        return el;
    };
    /**
     * @description 
     * Ajax () method is used to perform ajax (asynchronous HTTP requests
     * @param {Object} conf
     * @param {String} conf.type get/post, default get,required
     * @param {String} conf.url url parameters, required
     * @param {String} conf.data data parameters are optional, only need to be in the post request
     * @param {String} conf.dataType text/json/xml
     * @param {Function} conf.success callback
     */
    this.ajax = function (conf) {
        var xhr = null;
        if(window.XMLHttpRequest) {
            //Not IE browser
            xhr = new XMLHttpRequest();
        } else {
            try{
                //Series of IE browser
                xhr = new ActiveXObject('Microsoft.XMLHttp');
            }catch(e){
                xhr = new ActiveXObject('msxml2.xmlhttp');
            }
        }
        
        var type = conf.type;
        var url = conf.url;
        var data = conf.data;
        var dataType = conf.dataType;
        var success = conf.success;
        if (type == null){
            type = "get";
        }
        if (dataType == null){
            dataType = "text";
        }
        xhr.open(type, url, true);
        if (type.toUpperCase() == "GET") {
            xhr.send(null);
        } else if (type.toUpperCase() == "POST") {
            xhr.setRequestHeader("content-type","application/x-www-form-urlencoded");
            xhr.send(data);
        }
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if(dataType.toUpperCase() == "TEXT") {
                    if (success != null){
                        success(xhr.responseText);
                    }
                }else if(dataType.toUpperCase() == "XML") {
                    if (success != null){ 
                        success(xhr.responseXML);//XML
                    }  
                }else if(dataType.toUpperCase() == "JSON") {
                    if (success != null){
                        success(eval("("+xhr.responseText+")"));//Converts a json string to js object
                    }
                }
            }
        }
    }
}