#coding:utf-8
from flask import Flask
from flask import request
from flask import render_template
app = Flask(__name__,template_folder='templates',static_folder='static')

@app.route('/', methods=['GET', 'POST'])
def hello_world():
    return render_template('test.html')

@app.route('/login',methods=['GET','POST'])
def login():
	args = request.args
	if request.method == 'POST':
		args = request.form
	username = args.get('username',None)
	password = args.get('password',None)
	print 'username: %s password: %s' %(username,password)
	if username and password:
		return '{"success":true,"msg":"welcome %s .this text from service"}' %(username,)
	else:
		return '{"success":false,"msg":"username and password is null"}'
	
if __name__ == '__main__':
    app.run(debug=True, port=5000,host='0.0.0.0')